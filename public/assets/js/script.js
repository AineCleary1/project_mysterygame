const canvas = document.getElementById('the_canvas');
const ctx = canvas.getContext('2d');
let gameText = document.getElementById('game-text');

//variables
let screen = 0;
let mouseClick = false;
let doorS1 = false;
let text = "";
let textTyped = false;
let keynBatteryGot = false;
let keyShown = false;
let torchGot = false;
let torchShown = false;
let letterGot = false;
let bedClick = false;
let diaryGot = false;
let windowClick = false;
let ghostAni = false;
let playGame = false;
let instructions = false;
let bedLight_on = false;
let hallway_scene = false
let button = false;
let demoFin = false;
let bedScene = false;
let door9C = false;
let door9K = false;
let doorStairs = false;
let lampOn = false;

//scene text
let sc1Text = "You wake up in a strange room with no memory of how you got there, the room is filthy and looks like\n no one has stepped inside in years. You decide to investigate your surroundings.";
let sc2Text = "You walk out to a dimly lit corridor with multiple doors leading to other rooms. Faint whispers and creaking sounds can be heard from all directions. There seems to be more rooms on this floor, with an elevator at the end of the hall.";
let endText = "";

//sc1 object text
let sc1BedText = "An antique bed with a worn-out quilt. It seems untouched for years. A piece of paper sticks out from under a pillow. It looks like a page from a diary. It reads...";
let sc1DiaryText = "Dear Diary, This is my 3rd week in this hotel, and I hate it! Mother and Father are still away on their trip in Africa. Their last letter said that they will be getting the boat home next week. I hope the journey won't be delayed, as I have grown tired of the treatment of the staff toward me. I have no friends bar one bellhop named Mathew, he plays hide n' seek with me, and shows me secret rooms in the hotel! I must go now, grandmother will be waiting for me to start dinner. Love Emily";
let sc1DeskText = "A dusty desk with a few papers scattered on it. A torch is in one of the drawers. You place it in your pocket. A letter is found on top of the desk, among the scattered papers, addressed to a Mr. Knottingham. You start to read...";
let sc1LetterText = "Dear Emily, Your Mother and I hope you're having a good time with your Grandmother, while also behaving yourself. She doesn't have the energy to be running after you around that big hotel anymore, so be good and look after her. The next boat home will be leaving in a weeks time, so we will be on our way back to you soon. You might come along with us on our travels next time, your Mother misses you terribly when we are gone. With Love, Mother and Father";
let sc1WindowText = "Dirty windows with musty old curtains. It's dark outside, but you can see something moving outside.";
let sc1DrawerText = "A wooden drawer. The drawers come open with a strong pull. Inside a key, papers and rubbish is found, You place the key in your pocket.";
let sc1DoorLockedText = "A wooden door. It seems to be locked. Maybe a key will open it.";
let sc1DoorOpenText = "The key has unlocked the door. As it slowly creeks open, you step outside into a dimly lit hallway.";

//sc2 object text
let sc2Door9CText = "The first door on your right is closed. You turn the handle and give the door a shove to get it open. It doesn't budge.";
let sc2Door9BText = "The second door on your right is the bedroom you just left. Maybe you left something there...";
let sc2Door9KText = "The first door on your left is locked. Though as you stand beside it, you hear faint whispers coming from behind the door. As you attempt to turn the handle, the whispering stops, and a billow of dust comes from the bottom of the doorway. Maybe a key could unlock the door.";
let sc2StairsText = "A door with a sign Stairs written on it, off to the left of the hall. It won’t open either, maybe a key is needed.";
let sc2CartText = "An old trolly seems to be left in the hallway with some towels and other necessities. Wait, there seems to be a small golden object among the towels... It's and elevator button, this may come in handy.";
let sc2LiftText = "An old elevator is situated at the end of the hallway. It looks to be stopped on this current floor. You press the elevator button, and the doors start to open...";

//images
let splash_screen = new Image();
splash_screen.src = "assets/imgs/splashScreen.png";
let title_screen = new Image();
title_screen.src = "assets/imgs/startScreen.png";
let title_screen2 = new Image();
title_screen2.src = "assets/imgs/startScreen2.png";
//sc1
let sc1_bg = new Image();
sc1_bg.src = "assets/imgs/sc1/sc1_bedroom.png";
let sc1_bgNoShade = new Image();
sc1_bgNoShade.src = "assets/imgs/sc1/sc1_bedroomNoShade.png";
let sc1_bgNoV = new Image();
sc1_bgNoV.src = "assets/imgs/sc1/sc1_bedroomNoVinette.png";
let sc1_bgEmpty = new Image();
sc1_bgEmpty.src = "assets/imgs/sc1/sc1_bedroomEmpty.png";
let sc1_door = new Image();
sc1_door.src = "assets/imgs/sc1/sc1_Dopen.png";
let sc1_diary = new Image();
sc1_diary.src = "assets/imgs/sc1/sc1_diary.png"
let sc1_keyBattery = new Image();
sc1_keyBattery.src = "assets/imgs/sc1/sc1_keyBatteries.png";
let sc1_lampOn = new Image();
sc1_lampOn.src = "assets/imgs/sc1/sc1_lights.png";
let sc1_letter = new Image();
sc1_letter.src = "assets/imgs/sc1/sc1_letter.png";
let sc1_torch = new Image();
sc1_torch.src = "assets/imgs/sc1/sc1_torch.png";

//sc2
let sc2_bg = new Image();
sc2_bg.src = "assets/imgs/sc2/sc2_hallway.png";
let sc2_bgNoShade = new Image();
sc2_bgNoShade.src = "assets/imgs/sc2/sc2_bare-hall.png";
let sc2_bgNoShade2 = new Image();
sc2_bgNoShade2.src = "assets/imgs/sc2/sc2_bare-hall2.png";
let sc2_openLift = new Image();
sc2_openLift.src = "assets/imgs/sc2/sc2_open-lift.png";
let sc2_9bOpen = new Image();
sc2_9bOpen.src = "assets/imgs/sc2/sc2_9B-doorOpen.png";
let sc2_button = new Image();
sc2_button.src = "assets/imgs/sc2/sc2_button.png";
let sc2_lights = new Image();
sc2_lights.src = "assets/imgs/sc2/sc2_lights-all.png";
// //demo end
let demoEnd = new Image();
demoEnd.src = "assets/imgs/demo-end.png";

//audio
let splash_screen_audio = new Audio();
splash_screen_audio.src = "assets/audio/splashAudio.mp3";
let title_screen_audio = new Audio();
title_screen_audio.src = "assets/audio/titleAudio.mp3";
let game_screen_audio = new Audio();
game_screen_audio.src = "assets/audio/gameAudio.mp3";
let thunder_audio = new Audio();
thunder_audio.src = "assets/audio/thunder.mp3";

let screenPolygons = {
  1:[
    {name:'start game', points:[{x:0.50, y:0.30},{x:0.50, y:0.47},{x:0.69, y:0.47},{x:0.69, y:0.30}]},
    {name:'how to play', points:[{x:0.50, y:0.53},{x:0.50, y:0.70},{x:0.69, y:0.70},{x:0.69, y:0.53}]},
    {name:'rules X', points:[{x:0.67, y:0.11},{x:0.69, y:0.11},{x:0.69, y:0.14},{x:0.67, y:0.14}]},
  ],
  2:[
    {name:'bed sc1', points:[{x:0.55, y:0.67},{x:0.56, y:0.84},{x:0.85, y:0.94},{x:1, y:0.56},{x:1, y:0.27},{x:0.77, y:0.24},{x:0.76, y:0.26},{x:0.75, y:0.41},{x:0.57, y:0.67}]},
    {name:'drawer sc1', points:[{x:0.75, y:0.32},{x:0.75, y:0.40},{x:0.65, y:0.55},{x:0.63, y:0.54},{x:0.63, y:0.36},{x:0.66, y:0.30}]},
    {name:'window sc1', points:[{x:0.64, y:0.32},{x:0.46, y:0.30},{x:0.46, y:0.22},{x:0.49, y:0.00},{x:0.60, y:0.00},{x:0.65, y:0.27}]},
    {name:'desk sc1', points:[{x:0.43, y:0.30},{x:0.44, y:0.44},{x:0.41, y:0.46},{x:0.41, y:0.50},{x:0.35, y:0.48},{x:0.34, y:0.59},{x:0.31, y:0.62},{x:0.23, y:0.58},{x:0.23, y:0.38},{x:0.36, y:0.28}]},
    {name:'door sc1', points:[{x:0.15, y:0.10},{x:0.15, y:0.65},{x:0.05, y:0.75},{x:0.04, y:0.15}]},
  ],
  3:[
    {name:'stairs', points:[{x:0.19, y:0.19},{x:0.20, y:70},{x:0.25, y:0.63},{x:0.25, y:0.13}]},
    {name:'door 9K', points:[{x:0.05, y:0.32},{x:0.05, y:0.89},{x:0.12, y:0.80},{x:0.12, y:0.26}]},
    {name:'door 9C', points:[{x:0.87, y:0.26},{x:0.94, y:0.32},{x:0.94, y:0.89},{x:0.87, y:0.81}]},
    {name:'door 9B',points:[{x:0.74, y:0.13},{x:0.79, y:0.19},{x:0.79, y:0.70},{x:0.73, y:0.63}]},
    {name:'cart', points:[{x:0.59, y:0.37},{x:0.59, y:0.51},{x:0.65, y:0.60},{x:0.69, y:0.56},{x:0.69, y:0.42},{x:0.63, y:0.35}]},
    {name:'lift', points:[{x:0.41, y:0.08},{x:0.41, y:0.37},{x:0.57, y:0.37},{x:0.57, y:0.07}]},
  ]
};

//draws polygons for objects in game
function drawPolygons() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if(screenPolygons[screen]){
    screenPolygons[screen].forEach(polygon => {
      ctx.fillStyle = 'rgba(255, 0, 0, 0.5)';
      ctx.beginPath();
      ctx.moveTo(polygon.points[0].x * canvas.width, polygon.points[0].y * canvas.height);

      for (const point of polygon.points) {
        ctx.lineTo(point.x * canvas.width, point.y * canvas.height);
      }

      ctx.closePath();
      ctx.fill();
    });
  }
}

//gets mouse position on canvas
function getMousePos(event) {
  const rect = canvas.getBoundingClientRect();
  return {
    x: (event.clientX - rect.left) / rect.width,
    y: (event.clientY - rect.top) / rect.height,
  };
}

//checks for mouse 
function getHitPolygon(mousePos) {
  if(screenPolygons[screen]){
    for (const polygon of screenPolygons[screen]) {
      let inside = false;
  
      for (let i = 0, j = polygon.points.length - 1; i < polygon.points.length; j = i++) {
        const xi = polygon.points[i].x * canvas.width;
        const yi = polygon.points[i].y * canvas.height;
        const xj = polygon.points[j].x * canvas.width;
        const yj = polygon.points[j].y * canvas.height;
  
        const intersect = ((yi > mousePos.y * canvas.height) !== (yj > mousePos.y * canvas.height)) &&
          (mousePos.x * canvas.width < ((xj - xi) * (mousePos.y * canvas.height - yi)) / (yj - yi) + xi);
  
        if (intersect) {
          inside = !inside;
        }
      }
  
      if (inside) {
        return polygon.name;
      }
    }
  }
  return null;
}

//use setTimeout to transition from splash to title to game
function timelapse(){
  switch(screen){
    case 0: 
      //splash screen timer
      setTimeout(()=>{
        screen = 1;
      }, 5000); //3000 ms (3 secs
      break;
    case 1:
      if(playGame){
        setTimeout(()=>{
          screen = 2;
        }, 1000); //3000 ms (3 secs
      }
      break;
    case 2:
      //key timer
      if(keyShown){
        setTimeout(()=>{
          keyShown = false;
        }, 3000); //3000 ms (3 secs 
      }
      //bed text timer
      if(bedClick){
        setTimeout(()=>{
          bedClick = false;
          diaryGot = true;
        }, 3000);
      }
      //diary timer
      if(diaryGot){
        setTimeout(()=>{
          diaryGot = false;
        }, 30000);
      }
      //torch timer
      if(torchShown){
        setTimeout(()=>{
          torchShown = false;
          letterGot = true;
        }, 5000);
      }
      //letter timer
      if(letterGot){
        setTimeout(()=>{
          letterGot = false;
        }, 30000);
      }
      //new scene
      if(hallway_scene){
        setTimeout(()=>{
          screen = 3;
          bedScene = false;
        }, 3000);
      }
      break;
    case 3:
      //new scene
      if(demoFin){
        setTimeout(()=>{
          screen = 4;
        }, 3000);
      }
      //back to previous scene
      if(bedScene){
        setTimeout(()=>{
          screen = 2;
          doorS1 = false;
          hallway_scene = false;
        }, 3000);
      }
      //button shown
      if(button){
        setTimeout(()=>{
          button = false;
        }, 3000);
      }
      break;
} 
}
//to toggle sc1 lamp on and off
function toggleLamp() {
  if (screen === 2) { // Check if we are in scene 2 where the lamp effect is relevant
    // Determine the state of the lamp
    if (lampOn) {
      // Lamp is currently on, schedule it to turn off
      const offTime = Math.random() * (15000 - 5000) + 5000; // Random time between 5 and 15 seconds
      setTimeout(() => {
        lampOn = false;
        toggleLamp(); // Schedule the next toggle
      }, offTime);
    } else {
      // Lamp is currently off, schedule it to turn on
      const onTime = Math.random() * (20000 - 5000) + 5000; // Random time between 5 and 20 seconds
      setTimeout(() => {
        lampOn = true;
        toggleLamp(); // Schedule the next toggle
      }, onTime);
    }
  }
}

//function for playing game audio
function playMusic(){
  //pause all audio
  splash_screen_audio.pause();
  title_screen_audio.pause();
  game_screen_audio.pause();
  thunder_audio.pause();

  //play music for current scene
  switch(screen){
      case 0:
        splash_screen_audio.play();
        break;
      case 1:
        title_screen_audio.play();
        break;
      case 2:
        thunder_audio.play();
        game_screen_audio.play();
        break;
      case 3:
        game_screen_audio.play();
        break;
      case 4:
        game_screen_audio.pause();
        break;
  } 
}

//funtion to handle the interactions of the objects/polygons in game
function handleInteraction(polygonName){
  switch(screen){
    case 1:
      switch(polygonName){
        case 'start game':
          playGame = true;
          break;
        case 'how to play':
          instructions = true;
          break;
        case 'rules X':
          instructions = false;
          break;
      }
      break;
    case 2:
      switch(polygonName){
        case 'door sc1':
          doorS1 = true;
          //textTyped = false; //reset to allow new typing
          break;
        case 'bed sc1':
          bedClick = true;
          break;
        case 'drawer sc1':
          keynBatteryGot = true;
          keyShown = true;
          break;
        case 'window sc1':
          windowClick = true;
          break;
        case 'desk sc1':
          torchGot = true;
          torchShown = true;
          break;
      }
      break;
    case 3:
      switch(polygonName){
        case 'cart':
          button = true;
          break;
        case 'lift':
          demoFin = true;
          break;
        case 'door 9B':
          bedScene = true;
          break;
        case 'door 9K':
          door9K = true;
          break;
        case 'door 9C':
          door9C = true;
          break;
        case 'stairs':
          doorStairs = true;
          break;
      }
      break;
  }
}

//update function for setting timelapse, playmusic...
function update(){
  timelapse();

  switch(screen){
    case 0:
      playMusic(0);
      break;
    case 1:
      playMusic(1);
      break;
    case 2:
      playMusic(2);
      toggleLamp();
      break;
    case 3:
      playMusic(3);

      break;
    case 4:
      playMusic(4);
      break;
  }
}

//draw function for canvas
function draw(){
  switch(screen){
    case 0:
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.imageSmoothingEnabled = false; // Disable image smoothing
      ctx.drawImage(splash_screen,0,0,canvas.width,canvas.height);
      break;
    case 1:
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.imageSmoothingEnabled = false; // Disable image smoothing
      ctx.drawImage(title_screen,0,0,canvas.width,canvas.height);
      if(instructions){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.drawImage(title_screen2,0,0,canvas.width,canvas.height);
      }
      else{
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.drawImage(title_screen,0,0,canvas.width,canvas.height);
      }
      break;
    case 2:
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.imageSmoothingEnabled = false; // Disable image smoothing
      ctx.drawImage(sc1_bgNoV,0,0,canvas.width,canvas.height);
      ctx.drawImage(sc1_lampOn,0,0,canvas.width,canvas.height);
      gameText.textContent = sc1Text;
      //drawing open door
      if(doorS1){
        gameText.textContent = sc1DoorLockedText;
        //diaryGot = false;
        if(keynBatteryGot){
          ctx.clearRect(0,0,canvas.width,canvas.height);
          ctx.drawImage(sc1_bgNoShade,0,0,canvas.width,canvas.height);
          ctx.drawImage(sc1_door,0,0,canvas.width,canvas.height);
          gameText.textContent = sc1DoorOpenText;
          diaryGot = false;
          letterGot = false;
          hallway_scene = true;
        }
      }
      //drawing key+battery
      if(keyShown){
        ctx.drawImage(sc1_keyBattery,0,0,canvas.width,canvas.height);
        doorS1 = false;
        diaryGot = false;
        letterGot = false;
        windowClick = false;
        gameText.textContent = sc1DrawerText;
      }

      //drawing diary
      if(bedClick){
        gameText.textContent = sc1BedText;
        letterGot = false;
        windowClick = false;
      }
      if(diaryGot){
        ctx.drawImage(sc1_diary,0,0,canvas.width,canvas.height);
        gameText.textContent = sc1BedText;
      }

      //showing torch
      if(torchShown){
        ctx.drawImage(sc1_torch,0,0,canvas.width,canvas.height);
        gameText.textContent = sc1DeskText;
        diaryGot = false;
        windowClick = false;
      }

      //draw letter
      if(letterGot){
        diaryGot = false;
        ctx.drawImage(sc1_letter,0,0,canvas.width,canvas.height);
        gameText.textContent = sc1DeskText;
      }

      //draw ghost
      if(windowClick){
        gameText.textContent = sc1WindowText;
      }
      break;
    case 3: //hallway scene
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.imageSmoothingEnabled = false; // Disable image smoothing
      //draw bg image for screen 3
      ctx.drawImage(sc2_bg,0,0,canvas.width,canvas.height);
      ctx.drawImage(sc2_lights,0,0,canvas.width,canvas.height);
      gameText.textContent = sc2Text;
      //check for cart click
      if(button){
        ctx.drawImage(sc2_button,0,0,canvas.width,canvas.height);
        gameText.textContent = sc2CartText;
        door9C = false;
        door9K = false;
        doorStairs = false;
      }
      //door 9c click
      if(door9C){
        door9K = false;
        doorStairs = false;
        gameText.textContent = sc2Door9CText;
      }
      //door 9k click
      if(door9K){
        door9C = false;
        doorStairs = false;
        gameText.textContent = sc2Door9KText;
      }
      //stairs click
      if(doorStairs){
        door9C = false;
        door9K = false;
        gameText.textContent = sc2StairsText;
      }
      //lift clicked
      if(demoFin){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.imageSmoothingEnabled = false; // Disable image smoothing
        //draw bg image for screen 3
        ctx.drawImage(sc2_bgNoShade,0,0,canvas.width,canvas.height);
        ctx.drawImage(sc2_openLift,0,0,canvas.width,canvas.height);
        gameText.textContent = sc2LiftText;
      }

      //door 9b open
      if(bedScene){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.imageSmoothingEnabled = false; // Disable image smoothing
        //draw bg image for screen 3
        ctx.drawImage(sc2_bgNoShade2,0,0,canvas.width,canvas.height);
        ctx.drawImage(sc2_9bOpen,0,0,canvas.width,canvas.height);
        gameText.textContent = sc2Door9BText;
      }
      break;
    case 4: //end demo screen
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.imageSmoothingEnabled = false; // Disable image smoothing
      //draw bg image for screen 3
      ctx.drawImage(demoEnd,0,0,canvas.width,canvas.height);
      gameText.textContent = endText;
      break;
  }
}

//resize of canvas function for image quality
function resizeCanvas(){
  //set canvas based on css
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}

//event listener for mouse movement - possible removal at end
canvas.addEventListener('mousemove', (event) => {
  const mousePos = getMousePos(event);
  const hitPolygon = getHitPolygon(mousePos);

  console.clear();
  console.log(`Mouse X: ${mousePos.x.toFixed(2)}`);
  console.log(`Mouse Y: ${mousePos.y.toFixed(2)}`);
  console.log(`Inside Hit Polygon: ${hitPolygon || 'None'}`);
  console.log(screen);
  console.log(gameText);
});

//event listener for mouse clicks
canvas.addEventListener('mousedown', (event) => {
    const mousePos = getMousePos(event);
    const hitPolygon = getHitPolygon(mousePos);
    mouseClick = true;
    console.clear();
    console.log(`Mouse X click: ${mousePos.x.toFixed(2)}`);
    console.log(`Mouse Y click: ${mousePos.y.toFixed(2)}`);
    console.log(`Inside Click Polygon: ${hitPolygon || 'None'}`);
    console.log(mouseClick);

    if(hitPolygon){
      handleInteraction(hitPolygon); //handle interaction based on click
    }
});

//game loop function
function gameloop(){
    resizeCanvas();
    drawPolygons();
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}
window.onload = draw;
window.requestAnimationFrame(gameloop);