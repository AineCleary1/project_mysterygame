week 18th September
    
    Actions for the week
        -expanded on proposal for game, with a more detailed version of the story, gameplay in a new documnet.
        -looked into possible game engines that could be used for developing the game.
      
week 25th September

    Actions for the week
        -focused on a possible game engine, doing research on wether that game engine will be a good fit for the project.
        -started a research document, with the findings so far of the possible game engine.

week 2nd October

    Actions for the week
        -started focusing on creating a demo/prototype for the game, getting used to the chosen game engine and doing research to see if its a suitable choice for making the game.


week 9th October 

    Actions for the week
        -continued focusing on creating a demo/prototype for the game. 
        -getting used to the chosen game engine and doing research to see if its a suitable choice for making the game. 
        -Also started thinking about other documentation thats needed for this project, like a production spreadsheet, storyboard, script and so on.
        -Though most of these documentation dont need to be started until later in the project scheduel.
        -Came to a conclusion to hard code the game in Javascript where most of my knowledge of coding is, instead of trying to leanr a new game engine (UI, coding language included), hopefully speeding up the coding aspect for the game.


week 16th October

    Actions for the week
        -continued working on the demo for next week, with some interations, animations?, going from 1 scene to another.
        -did a quick production schedule for the project.
        -started thinking about starting both the presentation slides and game design doc.

week 23rd October

    Actions for the week
        -did a lot more of the coding for the demo due this wednesday.
        -went through the demo with my supervisor, and got some good feedback and what to focus on for the reading week.
        -focus on implimenting a new layout for the game, a new border with text box and inventory.
        -research and attempt to get a drag a drop feature working, to if its feesible.
        -work on getting some audio into the game.
        -add the necessary text animation for certain interactions for the game.
        -also work on the start of a presentation, and the extra documents that are needed for this project.

week 30th October

    Actions for the week
        -update documentation and work on a presentation for presenting project idea
        -work on an update of the art style, for the demo game to show art choice.
        -dropped idea of drag and drop feature as it would take the game from it's original simplistic idea.


week 6th November

    Actions for the week
        -continue working on the new art style for the demo game to be shown in the presentation.
        

week 13th November

    Action for the week
        -continue working on the presentation, needs to be ready for next week.
        -redo the title screen for the demo.
        -work on making a new asset for the note that needs both an animation and a letter written for the player to read at the start of the game to give an intro.
        -work on creating a new icon for the inventory, sound exit buttons.
        -work on a storyboard/paper prototype for the presentation.
        -do some research on how to accomplish a saving feature for the player + if it's possible to bring in a template for the website that the game will be based in.

week 20th November

    Action for the week
        -Finished off the presentation.
        -Completed the presentation, and got feedback from the supervisors. Certain things to do with the design and code need to be reviewed and researched in the following weeks.

week 27th November

    Action for the week
        -made an appointment with another supervisor for the following week to go over some of the coding and design aspects of this project.
        -continued to work on the storyboard and script for the project, making sure that the scale of the game that i set is achievable.

week 4th December

    Action for the week
        -updated the log as well as added more dialog for the script of the game.

week 11th December

    Action for the week
        -updated the log and continued to add to the script.
        -worked on exanding the storyline and all the interactions for the rooms on the floor.
        -made a quick floor plan of the hotel, and the floor that the game demo will focus on.

week 18th December - 14th January

    Action for the week
        -Christmas break: continued to focus on mainly the art work for the project.

week 15th January

    Action for the week
        -continued working on the 1st scene art, with some imput from my supervisor on what to change and fix.
        -started working on incorperating a some code from a prototype provided by a supervisor into my existing code, in order to help with getting more accurate object clicks for the gameplay.
        -contacted another supervisor to get help with issues invovling the code for the game.

week 22nd January

    Action for the week
        -again continued working on the 1st scene art. Focus on re-doing the carpet, so that it has a proper perspective for the room. change the size of the chandeliar to make it bigger. change the wall, to have a repetative print like wallpaper. the colour of the door needs to be darker/lighter than the scurting and floor.

week 29th January

    Action for the week
        -focused more on research for the coding aspect f the project. trying to come up with a solution for getting more accurate clicks on objects in the game. had a look at code provided by another supervisor, and got it working, but figured out some downsides and that it doesn't take responsiveness into account. went looking into researching librarys for solution.

week 5th February

    Action for the week
        -take a step back and see if there are better options for making the game. 
        -look at game engines again - unity, and other smaller engines.
        -look at changing game inputs, from clicks on canvas to click on buttons for options.

week 12th February

	Actions for the week
		-got some research help from another supervisor, in trying to tackle the better precision of object clicking in html canvas, while still being responsive.
		-tested the sample code to see if it would work, with mostly good outcomes so far. The solution seems solid, but other issues, such as the image rendering to the canvas isn't working well. Futher debugging and research needed to see of this solution will be implimented into the project.
		-still looking at other options for game development, as backup if the main goal of using js and html canvas keeps coming up with more issues.

week 19th February

	Action for the week
		-worked on getting the struckture of the first scene done, adding in all the objects, and trying to get the right atmosphere come across in the image.
		-got motes from the supervisor, to try and inprove the image, by adding more contrasting light to the scene, rimlight to enhance the mood and subtle patterns to make the overall image less plain.
		-started on the next scene, getting the outline and main structure of it done, thee is less to do for this scene in terms of objects, but further review is needed.

week 26th February

    Acttions for the week
        -finishing off the items for the 1st scene that would be found by the player, and putting them with a solid colour background to improve visibility of them.
        -also working on more details of the 2nd scene, like adding the doors and lighting, as well as a carpet design.
         

week 4th March

    Actions for the week
        -got more notes from supervisor to improve 1st scene, with adding different blending modes to certain layers for the wallpaper, as well as adding a vinette around the edges of the scene and fixing the lighting.
        -trying to fix the image output for the sample code of the game, doing some research on possible solutions, since known ones didnt improve it. If solution found, contact another supervisor for help with the code.

week 11th March

    Actions for the week
        -got the issue of the image not rendering without pixalation in the js code fixed.
        -also added all the coordinate points for the objects in the first scene.
        
week 18th March

    Actions for the week
        -continued working on polishing the first scene.
        -got some notes from the supervisor for things to work on/add for both 1st and 2nd scene over break.

week 25th March

    Actions for the week
        -finished 1st scene, and started exporting pngs for game.
        -got the click interactions set-up in code, along with scene changes and playing muisic.
        -worked on figuring out animated text, to appear for scene change and interactions.

week 1st April

    Actions for the week
        -nearly finished on 2nd scene art, just waiting on feedback and last minute additions that may be needed.
        -started on sc3 art, hopefully have it done the following week.
        -updated the start screen art and added it as another screen to the game code, and adding the buttons as polygons for game interactions.

week 8th April

    Actions for the week
        -got notes for adding new elements to the hallway scene and to mostly re-do the start screen.
        -added a new object and created animations for dorrs in hallway scene.
        -added trees, changed hotel outline and more mysterious atmosphere to the image with moon lighting and fog.

week 15th April

    Actions for the week
        -got more note to improve all artwork.
        -added better rim-light to an object in bedroom scene, added rimlight and more details as well as texture overlay to start screen.
        -fixed perspective of hallway with scurting and carpet, and adding needed object, with pop-up.
        -updated splash screen with my own image, and new text, while making a end of demo version too.
        -started exporting different versions of needed images for game code.
        -worked on adding the new screens of the hallway and the end of demo.
        -worked on getting all the text for all objects to display when needed.
        -tried to get the simple animations - flickering lights and ghost working, still progress for that.

week 22nd April

    Actions for the week
        -finished all art needed for the scenes in the demo and added it to the game.
        -the simple animations weren't working as expected, and so needed to be left as there wasn't time to try and fix the issues before the industry day.
        -gave the final presentation of the project, and discussing with the supervisors how the project went.
        -went through industry day, discussing my project with different employers that were interested.

week 29th April - 18th May

    Actions for the week
        -spent the next 3 weeks working on the final report and making sure all necessary documents and files are pushed up to my git repo.
