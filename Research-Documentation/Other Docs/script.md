scene1:
    Suite1~
        description -You wake up in a strange room with no memory of how you got there, the room is fithly and looks like no one has stepped inside in years. You decide to investigate your surroundings.
        drawer - A wooden drawer. The drawers come open with a strong pull. Inside a key, papers and rubbish is found, You place the key in your pocket.
        door(locked) -A wooden door. It seems to be locked. Maybe a key will open it.
        door(unlocked) - The key has unlocked the door. As it slowly creeks open, you step outside into a dimly lit hallway.
        window -Dirty windows with musty old curtains. It seems the window is open, as they flutter back and forth with the wind. It is pitch black, you cannot see anything outside.
        bed -An old, creaky bed. It doesn't look very comfortable.
        desk -A dusty desk with a few papers scattered on it. 
        letter -A letter that seems to have fallen to the floor, addressed to a Mr. Knottingham. You place it in your pocket.
        diary -A diary that was found among the clutter on the desk. It seems to have belonged to an Emily 

    Letter contents:

    Diary Contents:
        
